# Ansible configuration

Congestion Crowdsensing Project


## Overview

This Git repository contains [Ansible] definitions which aim to fully
describe the configuration of the project's infrastructure.


## Usage

Manually create a [YAML]-formatted file in
`group_vars/all/passwords.yml` with the following contents:

```yaml
---
nectar_vm_pass: jO3ClskAzH
db_pass: vEpr1WU671
```

substituting the actual passwords.  **Never commit this file.**  This
file path has been added to `.gitignore` to help prevent this
occurrence.

*   `nectar_vm_pass` is the password required to SSH into the NeCTAR
    VM.
*   `db_pass` is the password required by the CCP Collector web app to
    connect to the MySQL database.

Then, to apply the defined NeCTAR VM configuration to that machine,
install Ansible on a local machine and run this command from the root
directory of this repository:

```
$ ansible-playbook -i production nectar-vm.yml
```

The configuration definition is intended to be *idempotent*, i.e. it
should be okay to run the definition on a fully-configured machine
without any bad consequences.


[Ansible]: http://www.ansible.com/configuration-management
[YAML]: http://yaml.org/
